# PROJECT INFO

**TASK-MANAGER**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

| Описание | Ссылка |
|:----|:----|
| Скриншот исходной директории и запуск mvn clean package | https://yadi.sk/i/k5SjdlTEk9D43g |
| Результат работы mvn clean package | https://yadi.sk/i/zDgF7-ALNSOfUw |
| Результат запуска получившегося .jar | https://yadi.sk/i/glzcKcpg3Aorqw | 
